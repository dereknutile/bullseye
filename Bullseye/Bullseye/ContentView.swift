//
//  ContentView.swift
//  Bullseye
//
//  Created by Derek Nutile on 5/16/20.
//  Copyright © 2020 Derek Nutile. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
  @State var alertIsVisible: Bool = false
  @State var hitMeIsVisible: Bool = false
  @State var sliderValue: Double = 0.0
  @State var targetValue: Int = Int.random(in: 1...100)
  @State var gameScore: Int = 0
  @State var gameRound: Int = 1
  let midnightBlue = Color(red: 0.0/255.0, green: 51.0/255.0, blue: 102.0/255.0)
  
  struct customTextStyle: ViewModifier {
    func body(content: Content) -> some View {
      return content
        .foregroundColor(Color.white)
        .modifier(lightShadow())
        .font(Font.custom("Euphemia UCAS", size: 20))
    }
  }
  
  struct customValueStyle: ViewModifier {
    func body(content: Content) -> some View {
      return content
        .foregroundColor(Color.yellow)
        .modifier(lightShadow())
        .font(Font.custom("Euphemia UCAS", size: 22))
    }
  }
  
  struct lightShadow: ViewModifier {
    func body(content: Content) -> some View {
      return content
        .shadow(color: Color.black, radius: 5, x: 2, y: 2)
    }
  }
  
  struct buttonText: ViewModifier {
    func body(content: Content) -> some View {
      return content
        .foregroundColor(Color.white)
        .font(Font.custom("Euphemia UCAS", size: 16))
    }
  }
  
  struct buttonTextSmall: ViewModifier {
    func body(content: Content) -> some View {
      return content
        .foregroundColor(Color.white)
        .font(Font.custom("Euphemia UCAS", size: 12))
    }
  }
    
  var body: some View {
    VStack {
      Spacer()
      // Target row
      HStack {
        Text("Put the bullseye as close as you can to:").modifier(customTextStyle())
        Text("\(self.targetValue)").modifier(customValueStyle())
      }
      Spacer()
    
      // Slider row
      HStack {
        Text("1").modifier(customTextStyle())
        Slider(value: self.$sliderValue, in: 1...100)
        Text("100").modifier(customTextStyle())
      }
      Spacer()
      
      // Button row
      HStack {
        VStack {
          Button(action: {
              self.hitMeIsVisible = true
              
          }) {
              Text("Hit Me!").modifier(buttonText())
          }
          .alert(isPresented: $hitMeIsVisible) { () -> Alert in
            return Alert(
              title: Text(self.alertTitle()),
              message: Text("The slider value is: \(getSliderValue())\n" +
                "You earned \(self.pointsForCurrentRound()) points."
              ), dismissButton: .default(Text("Close")) {
                self.startNewRound()
              }
            )
          }
        }
        }.background(Image("Button")).modifier(lightShadow())
      Spacer()
      
      // Score row
      HStack {
        Button(action: {
            self.alertIsVisible = true
        }) {
          HStack{
            Image("StartOverIcon")
            Text("Start Over").modifier(buttonTextSmall())
          }
        }
        .alert(isPresented: $alertIsVisible) { () -> Alert in
          return Alert(
            title: Text("Start Over"),
            message: Text("Are you sure you want to start over?"),
            primaryButton: .default(Text("Yes")) {
              self.resetGame()
            },
            secondaryButton: .default(Text("No"))
          )
        }.background(Image("Button")).modifier(lightShadow())
        Spacer()
        Text("Score").modifier(customTextStyle())
        Text("\(self.gameScore)").modifier(customValueStyle())
        Spacer()
        
        Text("Round").modifier(customTextStyle())
        Text("\(self.gameRound)").modifier(customValueStyle())
        Spacer()
        
        NavigationLink(destination: AboutView()) {
          HStack{
            Image("InfoIcon")
            Text("Info").modifier(buttonTextSmall())
          }
        }.background(Image("Button")).modifier(lightShadow())
      }.padding(.bottom,20)
    }
    .background(Image("Background"), alignment: .center)
    .accentColor(midnightBlue)
    .navigationBarTitle("Bullseye")
  }
  
  //  Reset game
  func resetGame() {
    self.gameScore = 0
    self.gameRound = 1
    self.targetValue = Int.random(in: 1...100)
    self.sliderValue = 0.0
  }
  
  //  Start a new round
  func startNewRound() {
    self.calculateScore()
    self.targetValue = Int.random(in: 1...100)
    self.sliderValue = 0.0
    self.gameRound = self.gameRound + 1
  }
  
  //  Obtain the value of the slider
  func getSliderValue() -> Int {
    return Int(self.sliderValue.rounded())
  }
  
  //  Calculate the difference between the slider and the target and award points
  func pointsForCurrentRound() -> Int {
    let maximumPoints = 100
    let difference = abs(getSliderValue() - self.targetValue)
    let bonusPoints: Int
    if(difference == 0){
      bonusPoints = 100
    } else if(difference == 1) {
      bonusPoints = 50
    } else {
      bonusPoints = 0
    }
    
    return maximumPoints - difference + bonusPoints
  }
  
  //  Calculates the score after a round
  func calculateScore() {
    self.gameScore = self.gameScore + self.pointsForCurrentRound()
  }
  
  func alertTitle() -> String {
    let difference = abs(getSliderValue() - self.targetValue)
    let title: String
    if(difference == 0){
      title = "Perfect, you get 100 bonus points!"
    } else if(difference == 1){
      title = "Missed it by 1, you get 50 bonus points!"
    } else if(difference <= 5){
      title = "Oooh, so close!"
    } else if(difference <= 10){
      title = "Luke warm at best."
    } else {
      title = "Are you even trying?"
    }
    return title
  }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
      ContentView().previewLayout(.fixed(width: 896, height: 414))
    }
}
