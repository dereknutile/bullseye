//
//  AboutView.swift
//  Bullseye
//
//  Created by Derek Nutile on 6/5/20.
//  Copyright © 2020 Derek Nutile. All rights reserved.
//

import SwiftUI

let midnightBlue = Color(red: 0.0/255.0, green: 51.0/255.0, blue: 102.0/255.0)

struct customTextStyle: ViewModifier {
  func body(content: Content) -> some View {
    return content
      .foregroundColor(Color.white)
      .modifier(lightShadow())
      .font(Font.custom("Euphemia UCAS", size: 20))
  }
}

struct customValueStyle: ViewModifier {
  func body(content: Content) -> some View {
    return content
      .foregroundColor(Color.yellow)
      .modifier(lightShadow())
      .font(Font.custom("Euphemia UCAS", size: 22))
  }
}

struct lightShadow: ViewModifier {
  func body(content: Content) -> some View {
    return content
      .shadow(color: Color.black, radius: 5, x: 2, y: 2)
  }
}

struct buttonText: ViewModifier {
  func body(content: Content) -> some View {
    return content
      .foregroundColor(Color.white)
      .font(Font.custom("Euphemia UCAS", size: 16))
  }
}

struct buttonTextSmall: ViewModifier {
  func body(content: Content) -> some View {
    return content
      .foregroundColor(Color.white)
      .font(Font.custom("Euphemia UCAS", size: 12))
  }
}

struct AboutView: View {
    var body: some View {
      VStack{
        Spacer()
        Text("🎯 Bullseye 🎯").modifier(customTextStyle())
        Spacer()
        Text("The goal of Bullseye to to get the slider as close to the target as possible.  You can gain bonus points if you're really close!").modifier(customTextStyle())
        Spacer()
        Text("Good Luck!  - Derek").modifier(customTextStyle())
        Spacer()
      }
      .navigationBarTitle("About Bullseye")
      .background(Image("Background"), alignment: .center)
      .accentColor(midnightBlue)
    }
}

struct AboutView_Previews: PreviewProvider {
    static var previews: some View {
        AboutView().previewLayout(.fixed(width: 896, height: 414))
    }
}
